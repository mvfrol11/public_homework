module homework

go 1.18

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/chi/v5 v5.0.7
	github.com/google/uuid v1.3.0
	golang.org/x/sync v0.1.0
	gopkg.in/yaml.v2 v2.4.0
)
